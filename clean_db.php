<?php
include_once("./db_connect.php");


try {
    $conn = new PDO("mysql:host=$servername;dbname=avalon", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if($_GET["t"] == "departure"){
        $sql="TRUNCATE TABLE `avalon`.`departure_vote`";
    }
    else if($_GET["t"] == "mission"){
        $sql="TRUNCATE TABLE `avalon`.`misson_vote`";
    }
    
    $conn->query($sql);

    header("Location: ./home.php");



} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

?>