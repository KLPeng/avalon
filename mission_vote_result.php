<?php
    include_once('page_component/header.php');

    include_once("./db_connect.php");

    try {
        $conn = new PDO("mysql:host=$servername;dbname=avalon", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        $sql="SELECT * FROM `misson_vote` ORDER BY `random_number` DESC";

        $result = $conn->query($sql);

    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }

?>

<h2>投票結果：</h2>


<?php
    echo "<table border='1'>";
    echo "<tr><th>是否同意</th></tr>";
    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>{$row['mission_vote_result']}</td>";
        echo "</tr>";
    }
?>

<a href="./clean_db.php?t=mission"><button>返回主頁</button></a>



<?php
    include_once("page_component/footer.php");
?>