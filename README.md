# Avalon tool!

Hi! It's a simple Avalon tool that aims to allow players to play Avalon online!

Until now, this tool can perform simple voting, including anonymous and shuffling mechanisms.

I hope this tool can help you! Enjoy!
